function xuatra() {
    sum = 0;
    number = 0;

    while (sum < 10000) {
        //bước nhảy của vòng lặp là n++
        number++;
        //cứ sau mỗi vòng lặp thì tổng S sẽ được cộng dồn với n cho đến khi S > 10000 thì thoát khỏi vòng lặp
        sum += number;
    }

    document.getElementById("result1").innerHTML = "Số nguyên tố nhỏ nhất:" + number;

}

function tinhtong() {
    var x = document.getElementById("number1").value * 1;
    var n = document.getElementById("number2").value * 1;
    var sum = 0;
    var a = 1;

    for (var i = 1; i <= n; i++) {
        a = a * x;
        sum += a;
    }

    document.getElementById("result2").innerHTML = sum;
}

function giaithua() {
    var n = document.getElementById("number").value * 1;
    var giaithua = 1;

    if (n == 1) {
        giaithua = 1;
    } else {
        for (var i = 1; i <= n; i++) {
            giaithua = giaithua * i;
        }
    }

    document.getElementById("result3").innerHTML = "Giai thừa của " + n + " là: " + giaithua;

}


function taothediv() {
    var contentHTML = "";


    for (var i = 1; i <= 10; i++) {
        if (i % 2 != 0) {
            var contentLe = `<div class="bg-primary"> Div lẻ <br/></div>`;
            contentHTML += contentLe;
        } else {
            var contentChan = `<div class="bg-danger"> Div chẵn <br/></div>`;
            contentHTML += contentChan;
        }
    }
    document.getElementById("result4").innerHTML = contentHTML;

}

